package code;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

class UDPServer {
  public static void main(String[] args) throws Exception {
    int port = 5000;
    int packetLength = 2000;
    byte[] inputData = new byte[packetLength];
    byte[] outputData;
    DatagramSocket socket = new DatagramSocket(port);
    DatagramPacket packet = new DatagramPacket(inputData, inputData.length);
    Citation citations = new Citation();
    String chaine = "";
    String retour = "";
    do {
      socket.receive(packet);
      chaine = new String(packet.getData(), 0, packet.getLength());
      System.out.println("Message recu du client " + packet.getSocketAddress() + " : " + chaine);
      if(chaine.contains("CIT:")) {
        for(int i =0; i<Integer.parseInt(chaine.substring(4));i++) {
          retour+= "citations n" + (i+1) + ":\n";
          retour+=citations.getCitation();
          retour+="\n";
        }
      } else {
        retour = citations.getCitation();
    }
      outputData = retour.getBytes();
      packet.setData(outputData);
      socket.send(packet);
      System.out.print("Message envoyé au client "+ packet.getSocketAddress()+  "  : "+ retour);
    } while (!chaine.equals("fin"));
    socket.close();
  }
}
