package code;

import java.util.Scanner;
import java.io.FileReader;

class Citation {
  Scanner reader;

  public Citation() {
    try {
      reader = new Scanner(new FileReader("./motivations.txt"));
    } catch (Exception e) {
    }
  }

  public String getCitation() {
    String retour = "";
    for (int i =0; i<2; i++) {
      retour += reader.nextLine();
      retour += "\n";
    }
    return retour;
  }

  public String getEverything() {
    String retour = "";
    while (reader.hasNextLine()) {
      retour += reader.nextLine();
      retour += "\n";
    }
    return retour;
  }
}
